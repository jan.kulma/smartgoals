import { action, createModule, mutation } from 'vuex-class-component'
import { Goal, Objective } from '~/utils/models'

const VuexModule = createModule({
  namespaced: 'goals',
  strict: false,
  target: 'nuxt',
  enableLocalWatchers: true,
})

export class GoalsStore extends VuexModule {

  list: Goal[] = GoalsStore.initialize()

  get findByTitle () {
    return (title: string) => this.list.find(goal => goal.title === title)
  }

  @mutation SET (goals: Goal[]): void {
    this.list = goals
  }

  @mutation ADD (model: Goal): void {
    this.list.push(model)
  }

  @mutation REMOVE (goal: Goal): void {
    this.list.splice(this.list.indexOf(goal), 1)
  }

  @mutation UPDATE (model: Goal): void {
    const goal = this.list.find(goal => goal.id === model.id)
    Object.assign(goal, model)
  }

  @action
  async fetchAll () {
    const goal1 = new Goal()
    goal1.title = 'Goal 1'
    goal1.description = 'One must receive the body in order to avoid the lama of calm futility.'
    const objective1 = new Objective('Objective 1')
    objective1.objectives.push(new Objective('Objective 1a'))
    goal1.objectives.push(objective1)
    const goals = [goal1]
    this.SET(goals)
  }

  @action
  async add (goal: Goal) {
    this.ADD(goal)
  }

  @action
  async update (goal: Goal) {
    this.UPDATE(goal)
  }

  @action
  async remove (goal: Goal) {
    this.REMOVE(goal)
  }

  static $subscribe = {
    SET    (payload: any, state: any) { GoalsStore.refreshLocalStorage(state.goals.list) },
    ADD    (payload: any, state: any) { GoalsStore.refreshLocalStorage(state.goals.list) },
    UPDATE (payload: any, state: any) { GoalsStore.refreshLocalStorage(state.goals.list) },
    REMOVE (payload: any, state: any) { GoalsStore.refreshLocalStorage(state.goals.list) },
  }

  private static initialize (): Goal[] {
    let goals: Goal[] = []
    let goalsJSON = localStorage.getItem('goals')
    if (goalsJSON) {
      goals = JSON.parse(goalsJSON)
    }
    return goals
  }

  private static refreshLocalStorage (goals: Goal[]): void {
    localStorage.setItem('goals', JSON.stringify(goals))
  }
}
