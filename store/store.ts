import Vuex from 'vuex'
import { createProxy, extractVuexModule } from 'vuex-class-component'
import { GoalsStore } from '~/store/goals'

export const store = new Vuex.Store({
  modules: {
    ...extractVuexModule(GoalsStore)
  }
})

export const vxm = {
  goals: createProxy(store, GoalsStore),
}
