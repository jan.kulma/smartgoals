interface IHasObjectives {
  objectives: Objective[]
}

class Objective implements IHasObjectives {
  constructor (
    public name: string = '',
    public done: boolean = false,
    public objectives: Objective[] = []
  ) {}
}

class Goal implements IHasObjectives {
  constructor (
    public id: string = Math.random().toString(),
    public title: string = '',
    public description: string = '',
    public done: boolean = false,
    public due: Date = new Date(),
    public createdAt: Date = new Date(),
    public objectives: Objective[] = []
  ) {}
}

export {
  Goal, Objective, IHasObjectives
}
